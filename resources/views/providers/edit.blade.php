@extends('layouts.app')

	@section('title', 'Provider Edit')

		@section('content')

			{!! Form::model($provider, ['route' => ['providers.update',$provider],'method' => 'PUT', 'files' => true]) !!}
				@include('providers.form');

			{!!Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}

			{!!Form::close()!!}
	
	@endsection