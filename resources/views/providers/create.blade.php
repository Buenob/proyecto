@extends('layouts.app')

	@section('title', 'Providers Create')

		@section('content')

			@include('common.errors')

				{!! Form::open(['route' => 'providers.store', 'method' => 'POST', 'files' => true]) !!}

			@include('providers.form');
		
				{!!Form::submit('Guardar',['class' => 'btn btn-primary']) !!}


				{!!Form::close()!!}

	@endsection


